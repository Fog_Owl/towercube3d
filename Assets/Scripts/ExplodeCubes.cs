﻿using UnityEngine;

public class ExplodeCubes : MonoBehaviour
{	
	public GameObject restartButton, explotion;

	private bool colSet;

    private void OnCollisionEnter(Collision col)
    {
    	if(col.gameObject.tag == "Cube" && !colSet)
    	{
    		for(int i = col.transform.childCount - 1; i >= 0; i--)
    		{
    			Transform child = col.transform.GetChild(i);
    			child.gameObject.AddComponent<Rigidbody>();
    			child.gameObject.GetComponent<Rigidbody>().AddExplosionForce(70f, Vector3.up, 5f);
    			child.SetParent(null);
    		}
    		restartButton.SetActive(true);
    		Camera.main.transform.localPosition -= new Vector3(0f, 0f, 10f); // Отдаление камеры
            Camera.main.gameObject.AddComponent<CameraShake>();

            GameObject newVfx = Instantiate(explotion, new Vector3(col.contacts[0].point.x, col.contacts[0].point.y, col.contacts[0].point.z), Quaternion.identity) as GameObject;

            Destroy(newVfx, 2.5f);

            if (PlayerPrefs.GetString("music") != "No")
                GetComponent<AudioSource>().Play();

            Destroy(col.gameObject);
    		colSet = true;
    	}
    }
}
