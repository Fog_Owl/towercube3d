﻿using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions.Must;

public class CanvasButtons : MonoBehaviour
{
	public Sprite musicOn, musicOff;

    private void Start()
    {
		if (PlayerPrefs.GetString("music") == "No" && gameObject.name == "Music")
			GetComponent<Image>().sprite = musicOff;


        if (PlayerPrefs.GetString("musicMain") == "No")
			Camera.main.GetComponent<AudioSource>().mute = true;
		else
			Camera.main.GetComponent<AudioSource>().mute = false;


	}


    public void RestartGame()
	{
		if (PlayerPrefs.GetString("music") != "No")
			GetComponent<AudioSource>().Play();


		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	public void  LoadInstagram()
	{
		if (PlayerPrefs.GetString("music") != "No")
			GetComponent<AudioSource>().Play();

		Application.OpenURL("https://www.instagram.com/mefirstgames/");
	}

	public void MusicWork()
    {
        if (PlayerPrefs.GetString("music") == "No")
        {
			PlayerPrefs.SetString("musicMain","Yes");
			Camera.main.GetComponent<AudioSource>().mute = false;
			PlayerPrefs.SetString("music", "Yes");
			GetComponent<Image>().sprite = musicOn;
        }
		else
        {
			
			Camera.main.GetComponent<AudioSource>().mute = true;
			PlayerPrefs.SetString("music", "No");
			PlayerPrefs.SetString("musicMain", "No");
			GetComponent<Image>().sprite = musicOff;

		}
	}
}
