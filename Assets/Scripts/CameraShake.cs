﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    private Transform camTransform;
    private float shakeDuration = 1f,
                  shakeAmount = 0.04f,
                  decreaseFactor = 1.5f;

    private Vector3 originPos;

    private void Start()
    {
        camTransform = GetComponent<Transform>();
        originPos = camTransform.localPosition;
    }

    private void Update()
    {
        if (shakeDuration > 0)// Пока тряска
        {
            camTransform.localPosition = originPos + Random.insideUnitSphere * shakeAmount; // Меняем позицию камеры
            shakeDuration -= Time.deltaTime * decreaseFactor;
        }
        else
        {
            shakeDuration = 0;
            camTransform.localPosition = originPos;
        }
    }

}
